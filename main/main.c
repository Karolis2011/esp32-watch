#include "deviceDefines.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include <time.h>
#include <sys/time.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "string.h"
#include "SSD1331.h"
#include "espGFX.h"
#include "fonts/freemono.h"
#include "fonts/Picopixel.h"
#include "lwip/err.h"
#include "apps/sntp/sntp.h"

static uint16_t testing_icon[] = {
  0xF900, 0x0000, 0xF900, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0xF900, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0xFFFF, 0xFFFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0xF900, 0x0000, 0xF900, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0xFFFF,
  0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x07E0, 0x07E0, 0x0000, 0x0000, 0x07E0,
  0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0,
  0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0,
  0x07E0, 0x07E0, 0x0000, 0x0000, 0x0000, 0x07E0, 0x07E0, 0x07E0, 0x07E0, 0x0000, 0x07E0, 0x07E0, 0x07E0, 0x0000, 0x0000, 0x07E0,
  0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0,
  0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x0000,
  0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0, 0x0000, 0x0000, 0x07E0, 0x0000, 0x07E0
};

extern const uint8_t roboto_regular_start[] asm("_binary_Roboto_Regular_ttf_start");
extern const uint8_t roboto_regular_end[]   asm("_binary_Roboto_Regular_ttf_end");

/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t wifi_event_group;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int CONNECTED_BIT = BIT0;

static void obtain_time(void);
static void initialize_sntp(void);
static void initialise_wifi(void);
static esp_err_t event_handler(void *ctx, system_event_t *event);

static void initialise_wifi(void)
{
  tcpip_adapter_init();
  wifi_event_group = xEventGroupCreate();
  ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
  ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
  wifi_config_t wifi_config = {
    .sta = {
      .ssid = "HP-40-2",
      .password = "iatad2gvvzvq10qo1jpj",
    },
  };
  ESP_LOGI("WIFI-INIT", "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);
  ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
  ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
  ESP_ERROR_CHECK( esp_wifi_start() );
}

static void initialize_sntp(void)
{
  ESP_LOGI("SNTP", "Initializing SNTP");
  sntp_setoperatingmode(SNTP_OPMODE_POLL);
  sntp_setservername(0, "pool.ntp.org");
  sntp_init();
}

static void obtain_time(void)
{
  ESP_ERROR_CHECK( nvs_flash_init() );
  initialise_wifi();
  xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
                      false, true, portMAX_DELAY);
  initialize_sntp();

  // wait for time to be set
  time_t now = 0;
  struct tm timeinfo = { 0 };
  int retry = 0;
  const int retry_count = 10;
  while(timeinfo.tm_year < (2016 - 1900) && ++retry < retry_count) {
      ESP_LOGI("OBTAIN", "Waiting for system time to be set... (%d/%d)", retry, retry_count);
      vTaskDelay(2000 / portTICK_PERIOD_MS);
      time(&now);
      localtime_r(&now, &timeinfo);
  }

  ESP_ERROR_CHECK( esp_wifi_stop() );
}

esp_err_t event_handler(void *ctx, system_event_t *event)
{
  switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
      esp_wifi_connect();
      break;
    case SYSTEM_EVENT_STA_GOT_IP:
      xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      /* This is a workaround as ESP32 WiFi libs don't currently
          auto-reassociate. */
      esp_wifi_connect();
      xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
      break;
    default:
      break;
  }
  return ESP_OK;  
}

static wchar_t* charToWChar(const char* text)
{
    const size_t size = strlen(text) + 1;
    wchar_t* wText = malloc(size * sizeof(wchar_t));
    mbstowcs(wText, text, size);
    return wText;
}

void app_main(void)
{
  ESP_LOGI("CLear", "Start draw test");
  esp_err_t ret;
  spi_device_handle_t oledSpi;
  spi_bus_config_t buscfg = {
    .miso_io_num = PIN_NUM_DEVICE_MISO,
    .mosi_io_num = PIN_NUM_DEVICE_MOSI,
    .sclk_io_num = PIN_NUM_DEVICE_CLK,
    .quadwp_io_num = -1,
    .quadhd_io_num = -1
  };
  spi_device_interface_config_t oledcfg = {
    .clock_speed_hz=10000000,
    .mode=0,
    .spics_io_num=PIN_NUM_DEVICE_OLED_CS,
    .queue_size=1,
  };
  ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
  assert(ret == ESP_OK);
  ret = spi_bus_add_device(HSPI_HOST, &oledcfg, &oledSpi);
  assert(ret == ESP_OK);
  SSD1331_handle_t dsp = {
    .spi = &oledSpi,
    .PIN_DS = PIN_NUM_DEVICE_OLED_DC,
    .PIN_RST = PIN_NUM_DEVICE_OLED_RST,
    .width = OLED_WIDTH,
    .height = OLED_HEIGHT,
    .rotation = 1,
  };
  GFX_init((char*)&dsp);

  GFX_clear();
  //GFX_drawRGBBitmap(0, 20, &testing_icon[0], 16, 16);
  //GFX_drawChar(0, 11, 'K', toColor565(255, 0, 0), &FreeMono9pt7b);
  //GFX_drawChar(11, 11, 'a', toColor565(0, 255, 0), &FreeMono9pt7b);
  //GFX_drawChar(22, 11, 'r', toColor565(0, 0, 255), &FreeMono9pt7b);
  //GFX_drawText(0, 47, "LabaĄČĘĖĮŠŲŪŽąčęėįšųūž", toColor565(0, 0, 255), &Picopixel);
  //GFX_fillScreen(COLOR_BLUE);
  GFX_initFont(roboto_regular_start, "roboto");
  GFX_drawTextNew(0, 10, L"Hello world", toColor565(255, 0, 0), "roboto", 14);
  GFX_drawTextNew(0, 30, L"How thing's going", toColor565(0, 255, 0), "roboto", 14);
  GFX_drawTextNew(0, 58, charToWChar(__DATE__), toColor565(255, 255, 255), "roboto", 14);
  GFX_fillRect_A(12, 16, 20, 20, toColor5658(255, 0, 0, 150));
  GFX_fillRect_A(0, 8, 20, 20, toColor5658(0, 0, 255, 100));
  //GFX_drawTextNew(40, 16, L"Šiauliai", toColor565(255, 0, 0), "roboto", 14);
  //GFX_drawTextNew(30, 32, L"Ažuolas Ačiū", toColor565(0, 255, 0), "roboto", 14);
  //GFX_drawTextNew(30, 48, L"Kąstytis lietuvaitė", toColor565(0, 0, 255), "roboto", 14);

  //vTaskDelay(1000/portTICK_PERIOD_MS);
  //GFX_clear();
  //GFX_drawTextNew(0, 14, L"Initializing...", toColor565(255, 0, 0), "roboto", 14);
  // stbtt_fontinfo font;
  // int c = 0x0160;
  // int s = 12;
  // unsigned char *bitmap;
  // stbtt_InitFont(&font, roboto_regular_start, stbtt_GetFontOffsetForIndex(roboto_regular_start,0));
  // ESP_LOGI("TT", "printimg %c", c);
  // ESP_LOGI("TT", "init");
  // GFX_drawCharNew(50, 0, c, 0, &font, stbtt_ScaleForPixelHeight(&font, s));
  // bitmap = stbtt_GetCodepointBitmap(&font, 0,stbtt_ScaleForPixelHeight(&font, s), c, &w, &h, 0,0);
  // ESP_LOGI("TT", "render");
  // for (int j=0; j < h; ++j) {
  //    for (int i=0; i < w; ++i) {
  //      if(bitmap[j*w+i] > 20) {
  //        ESP_LOGI("TT", "%d", bitmap[j*w+i]);
  //        GFX_drawPixel(50 + i, j, toColor565(0, bitmap[j*w+i], bitmap[j*w+i]));
  //        vTaskDelay(1000/portTICK_PERIOD_MS);
  //      }
  //    }
  // }

  ESP_LOGI("TT", "draw");

  /*
  FT_Init_FreeType(&ftlib);
  FT_New_Memory_Face(ftlib, roboto_regular_start, (roboto_regular_end - roboto_regular_start), 0, &face);
  FT_Set_Char_Size(
    face,
    0,
    16*64,
    OLED_DENSITY,
    OLED_DENSITY );
  FT_Set_Pixel_Sizes(
    face,
    0,
    16 );
  int glyph_index = FT_Get_Char_Index(face, 352);
  FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER );
  GFX_drawBitmap(50, 0, face->glyph->bitmap.buffer, face->glyph->bitmap.width, face->glyph->bitmap.rows, toColor565(255, 255, 0));
  */
  // for (int y = 0; y < 16; y++) {
  //   for (int x = 0; x < 16; x++) {
  //     GFX_drawPixel(x, y + 20, toColor565(0, testing_icon[x+(y*16)], 0));
  //   }
  // }
  // vTaskDelay(1500/portTICK_PERIOD_MS);S
  // int pix = 0;
  // for (int y = 0;y < OLED_HEIGHT; y++) {
  //   for (int i = 0;i < OLED_WIDTH; i++) {
  //     uint16_t c = 0;
  //     if(pix) {
  //       c = toColor565(255, 0, 0); // Red
  //     } else {
  //       c = toColor565(0, 255, 0); // Green
  //     }
  //     SSD1331_drawPixel(&dsp, i, y, c);
  //     pix = !pix;
  //   }
  // }
  // SSD1331_drawPixel(&dsp, 50, 50, toColor565(255, 255, 255));
  // int y = 0;
  // while(1) {
  //   for(int r = 0; r < 256; r+= 8) {
  //     for(int g = 0; g < 256; g += 8) {
  //       for(int b = 0; b < 256; b += 8) {
  //         SSD1331_drawLine(&dsp, 0, y, OLED_WIDTH - 1, y, toColor565(r, g, b));
  //         y++;
  //         if(y > OLED_HEIGHT) {
  //           y = 0;
  //         }
  //         //SSD1331_goTo(&dsp, cnt % OLED_WIDTH, cnt / OLED_HEIGHT);
  //         //SSD1331_drawPixel(&dsp, cnt % OLED_WIDTH, cnt / OLED_HEIGHT, toColor565(r, g, b));
  //         //vTaskDelay(1/portTICK_PERIOD_MS);
  //         // cnt++;
  //         // if(cnt > OLED_WIDTH * OLED_WIDTH) {
  //         //   cnt = 0;
  //         // }
  //       }
  //     }
  //   }

  //}
  //SSD1331_clearScreen(&dsp);
  //while(1) {
  //  SSD1331_drawLine(&dsp, 0, 0, 50, 50, toColor565(255, 0, 0));
  //  vTaskDelay(300/portTICK_PERIOD_MS);
  //}
  // while (1) {
  //   GFX_fillRect_A(0, 50, 96, 14, toColor5658(0, 255, 0, 50));
  //   GFX_fillRect_A(0, 8, 20, 20, toColor5658(0, 0, 255, 70));
  //   GFX_fillRect_A(0, 0, 96, 64, toColor5658(255, 255, 255, 10)); //setenv("TZ", "Europe/Vilnius", 1);
  // }
  vTaskDelay(2000/portTICK_PERIOD_MS);
  GFX_clear();
  ESP_LOGI("CLear", "Clear");
  vTaskDelay(2000/portTICK_PERIOD_MS);
  time_t now;
  struct tm timeinfo;
  time(&now);
  localtime_r(&now, &timeinfo);
  // Is time set? If not, tm_year will be (1970 - 1900).
  if (timeinfo.tm_year < (2016 - 1900)) {
      ESP_LOGI("TIME", "Time is not set yet. Connecting to WiFi and getting time over NTP.");
      obtain_time();
      // update 'now' variable with current time
      time(&now);
  }
  char strftime_buf[64];
  setenv("TZ", "EET-2EEST", 1);
  tzset();
  localtime_r(&now, &timeinfo);
  size_t len = strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
  wchar_t* dataL = malloc((len - 1) * sizeof(wchar_t));
  mbstowcs(dataL, strftime_buf, len - 1);
  ESP_LOGI("TIME", "The current date/time in Vilnius is: %s", strftime_buf);
  GFX_drawTextNew(0, 14, dataL, toColor565(255, 255, 255), "roboto", 14);
  vTaskDelay(20000/portTICK_PERIOD_MS);
}