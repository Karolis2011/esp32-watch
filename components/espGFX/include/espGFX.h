#include "gfxfont.h"
#include "wchar.h"

uint16_t toColor565(uint8_t r, uint8_t g, uint8_t b);
uint32_t toColor5658(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
void GFX_clear();
void GFX_drawPixel(int16_t x, int16_t y, uint16_t color);
void GFX_drawPixel_A(int16_t x, int16_t y, uint32_t alphColor);
void GFX_drawBitmap(int16_t x, int16_t y, uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);
void GFX_drawRGBBitmap(int16_t x, int16_t y, uint16_t *bitmap, int16_t w, int16_t h);
void GFX_drawMaskedRGBBitmap(int16_t x, int16_t y, uint16_t *bitmap, uint8_t *mask, int16_t w, int16_t h);
void GFX_drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color, const GFXfont *font);
void GFX_drawText(int16_t x, int16_t y, char * text, uint16_t color, const GFXfont *font);
void GFX_initFont(const uint8_t * font_file, char * name);
void GFX_drawTextNew(int16_t x, int16_t y, wchar_t * text, uint16_t color, char * font, int size);
void GFX_init(char * displayPointer);
void GFX_drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
void GFX_drawLine_A(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint32_t color);
void GFX_fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
void GFX_fillRect_A(int16_t x, int16_t y, int16_t w, int16_t h, uint32_t color);
void GFX_fillScreen(uint16_t color);

#define COLOR_RED 0xF800
#define COLOR_GREEN 0x07E0
#define COLOR_BLUE 0x001F
#define COLOR_WHITE 0xFFFF
#define COLOR_BLACK 0x0

#ifndef _swap_int16_t
#define _swap_int16_t(a, b) { int16_t t = a; a = b; b = t; }
#endif
