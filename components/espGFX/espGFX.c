#include "stdint.h"
#include "ctype.h"
#include "wchar.h"
#include "deviceDefines.h"
#include "gfxfont.h"
#include "espGFX.h"
#if defined SCREEN_SSD1331
  #include "SSD1331.h"
  static SSD1331_handle_t * mainDisplay;
#endif

#define STB_TRUETYPE_IMPLEMENTATION  // force following include to generate implementation
#include "stb_truetype.h"

uint16_t displayBuffer[OLED_WIDTH][OLED_HEIGHT];

#define GFX_MAX_FONTS 64
static char * font_names[GFX_MAX_FONTS];
static stbtt_fontinfo * font_ptrs[GFX_MAX_FONTS];

uint16_t toColor565(uint8_t r, uint8_t g, uint8_t b) {
  uint16_t c;
  c = r >> 3;
  c <<= 6;
  c |= g >> 2;
  c <<= 5;
  c |= b >> 3;
  return c;
}

uint32_t toColor5658(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
  uint32_t c;
  c = a;
  c <<= 5;
  c |= r >> 3;
  c <<= 6;
  c |= g >> 2;
  c <<= 5;
  c |= b >> 3;
  return c;
}

#define MASK_RB       63519 // 0b1111100000011111
#define MASK_G         2016 // 0b0000011111100000
#define MASK_MUL_RB 4065216 // 0b1111100000011111000000
#define MASK_MUL_G   129024 // 0b0000011111100000000000
#define MAX_ALPHA        64 // 6bits+1 with rounding

uint16_t alphablend(uint16_t fg, uint16_t bg, uint8_t alpha) {

  // alpha for foreground multiplication
  // convert from 8bit to (6bit+1) with rounding
  // will be in [0..64] inclusive
  alpha = ( alpha + 2 ) >> 2;
  // "beta" for background multiplication; (6bit+1);
  // will be in [0..64] inclusive
  uint8_t beta = MAX_ALPHA - alpha;
  // so (0..64)*alpha + (0..64)*beta always in 0..64

  return (uint16_t)((
            (  ( alpha * (uint32_t)( fg & MASK_RB )
                + beta * (uint32_t)( bg & MASK_RB )
            ) & MASK_MUL_RB )
          |
            (  ( alpha * ( fg & MASK_G )
                + beta * ( bg & MASK_G )
            ) & MASK_MUL_G )
         ) >> 6 );
}

void GFX_clear() {
  memset(displayBuffer, 0, sizeof(displayBuffer[0][0]) * OLED_WIDTH * OLED_HEIGHT);
  #if defined SCREEN_SSD1331
    SSD1331_clearScreen(mainDisplay);
  #endif
}

void GFX_drawPixel(int16_t x, int16_t y, uint16_t color) {
  displayBuffer[x][y] = color;
  #if defined SCREEN_SSD1331
    SSD1331_drawPixel(mainDisplay, x, y, color);
  #endif
}

void GFX_drawPixelAlpha(int16_t x, int16_t y, uint16_t color, uint8_t alpha) {
  GFX_drawPixel(x, y, alphablend(color, displayBuffer[x][y], alpha));
}

void GFX_drawPixel_A(int16_t x, int16_t y, uint32_t alphColor) {
  uint16_t color = alphColor & 0xFFFF;
  uint8_t alpha = (alphColor & 0xFF0000) >> 16;
  GFX_drawPixelAlpha(x, y, color, alpha);
}

void GFX_drawBitmap(int16_t x, int16_t y, uint8_t *bitmap, int16_t w, int16_t h, uint16_t color) {
    int16_t byteWidth = (w + 7) / 8;
    uint8_t byte = 0;
    for(int16_t j=0; j<h; j++, y++) {
        for(int16_t i=0; i<w; i++ ) {
            if(i & 7) byte <<= 1;
            else      byte   = bitmap[j * byteWidth + i / 8];
            if(byte & 0x80) GFX_drawPixel(x+i, y, color);
        }
    }
}

void GFX_drawRGBBitmap(int16_t x, int16_t y, uint16_t *bitmap, int16_t w, int16_t h) {
  for(int16_t j=0; j<h; j++, y++) {
    for(int16_t i=0; i<w; i++ ) {
      GFX_drawPixel(x+i, y, bitmap[j * w + i]);
    }
  }
}

void GFX_drawMaskedRGBBitmap(int16_t x, int16_t y, uint16_t *bitmap, uint8_t *mask, int16_t w, int16_t h) {
    int16_t bw   = (w + 7) / 8;
    uint8_t byte = 0;
    for(int16_t j=0; j<h; j++, y++) {
        for(int16_t i=0; i<w; i++ ) {
            if(i & 7) byte <<= 1;
            else      byte   = mask[j * bw + i / 8];
            if(byte & 0x80) {
                GFX_drawPixel(x+i, y, bitmap[j * w + i]);
            }
        }
    }
}

void GFX_drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color, const GFXfont *font) {
  c -= (uint8_t)font->first;
  GFXglyph *glyph = &(((GFXglyph *)font->glyph)[c]);
  uint8_t *bitmap = (uint8_t *)font->bitmap;
  uint16_t bo = glyph->bitmapOffset;
  uint8_t  w  = glyph->width,
           h  = glyph->height;
  int8_t   xo = glyph->xOffset,
           yo = glyph->yOffset;
  uint8_t  xx, yy, bits = 0, bit = 0;
  for(yy=0; yy<h; yy++) {
    for(xx=0; xx<w; xx++) {
      if(!(bit++ & 7)) {
        bits = bitmap[bo++];
      }
      if(bits & 0x80) {
        GFX_drawPixel(x+xo+xx, y+yo+yy, color);
      }
      bits <<= 1;
    }
  }
}

void GFX_drawText(int16_t x, int16_t y, char * text, uint16_t color, const GFXfont *font) {
  int16_t relative_x = 0;
  int16_t relative_y = 0;
  for (int i = 0; text[i] != 0; i++) {
    char c = text[i];
    if((c != '\n') || (c != '\r')) {
      uint8_t first = (uint8_t)font->first;
      uint8_t last = (uint8_t)font->last;
      if((c >= first) && (c <= last)) {
        GFXglyph *glyph = &(((GFXglyph *)font->glyph)[c - first]);
        uint8_t  w  = glyph->width,
                 h  = glyph->height;
        if((w > 0) && (h > 0)) {
          //TODO:Word wraping and new lines
          GFX_drawChar(x + relative_x, y + relative_y, c, color, font);
        }
        relative_x += (uint8_t)glyph->xAdvance;
        //cursor_x += (uint8_t)pgm_read_byte(&glyph->xAdvance) * (int16_t)textsize;
      }
    }
  }
}

void GFX_initFont(const uint8_t * font_file, char * name) {
  int fi = 0;
  do {
    fi++;
  } while(font_names[fi]);
  font_names[fi] = name;
  stbtt_fontinfo * font = malloc(sizeof(stbtt_fontinfo));
  stbtt_InitFont(font, font_file, stbtt_GetFontOffsetForIndex(font_file, 0));
  font_ptrs[fi] = font;
}

void GFX_drawCharNew(int16_t x, int16_t y, int c, uint16_t color, const stbtt_fontinfo *font, float scale) {
  unsigned char *bitmap;
  int w, h, xo, yo;
  bitmap = stbtt_GetCodepointBitmap(font, 0, scale, c, &w, &h, &xo, &yo);
  for (int yy = 0; yy < h; yy++) {
    for (int xx = 0; xx < w; xx++) {
      if(bitmap[yy*w+xx] > 10) {
        GFX_drawPixelAlpha(x+xo+xx, y+yo+yy, color, bitmap[yy*w+xx]);
        //GFX_drawPixel(x+xo+xx, y+yo+yy, alphablend(color, 0, bitmap[yy*w+xx]));
      }
    }
  }
}

void GFX_drawTextNew(int16_t x, int16_t y, wchar_t * text, uint16_t color, char * font, int size) {
  int fi = 0;
  do {
    fi++;
  } while(strcmp(font, font_names[fi]) && font_names[fi]);
  if(!font_names[fi]) {
    //No font found
    return;
  }
  stbtt_fontinfo * fonti = font_ptrs[fi];
  int16_t relative_x = 0;
  int16_t relative_y = 0;
  float scale = stbtt_ScaleForPixelHeight(font_ptrs[fi], size);
  int ascent;
  stbtt_GetFontVMetrics(fonti, &ascent, 0, 0); //Ascent, Descent and lineGap
  for (int i = 0; text[i] != 0; i++) {
    int c = text[i];
    if(c == 0) {
      return; //End of string
    }
    if((c != '\n') || (c != '\r')) {
      int advance, xbar;
      stbtt_GetCodepointHMetrics(fonti, c, &advance, &xbar);
      GFX_drawCharNew(relative_x + x + xbar * scale, relative_y + y, c, color, fonti, scale);
      relative_x += advance * scale;
    }
  }

}

void GFX_drawLine_S(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color) {
  int16_t steep = abs(y1 - y0) > abs(x1 - x0);
  if (steep) {
    _swap_int16_t(x0, y0);
    _swap_int16_t(x1, y1);
  }
  if (x0 > x1) {
    _swap_int16_t(x0, x1);
    _swap_int16_t(y0, y1);
  }

  int16_t dx, dy;
  dx = x1 - x0;
  dy = abs(y1 - y0);

  int16_t err = dx / 2;
  int16_t ystep;

  if (y0 < y1) {
    ystep = 1;
  } else {
    ystep = -1;
  }

  for (; x0<=x1; x0++) {
    if (steep) {
      GFX_drawPixel(y0, x0, color);
    } else {
      GFX_drawPixel(x0, y0, color);
    }
    err -= dy;
    if (err < 0) {
      y0 += ystep;
      err += dx;
    }
  }
}

void GFX_drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color) {
  #if defined SCREEN_SSD1331
    SSD1331_drawLine(mainDisplay, x0, y0, x1, y1, color); //HW aceleration
    int16_t steep = abs(y1 - y0) > abs(x1 - x0);
    if (steep) {
      _swap_int16_t(x0, y0);
      _swap_int16_t(x1, y1);
    }
    if (x0 > x1) {
      _swap_int16_t(x0, x1);
      _swap_int16_t(y0, y1);
    }

    int16_t dx, dy;
    dx = x1 - x0;
    dy = abs(y1 - y0);

    int16_t err = dx / 2;
    int16_t ystep;

    if (y0 < y1) {
      ystep = 1;
    } else {
      ystep = -1;
    }

    for (; x0<=x1; x0++) {
      if (steep) {
        displayBuffer[y0][x0] = color;
      } else {
        displayBuffer[x0][y0] = color;
      }
      err -= dy;
      if (err < 0) {
        y0 += ystep;
        err += dx;
      }
    }
  #else
    GFX_drawLine_S(x0, y0, x1, y1, color);
  #endif
}

void GFX_drawLine_A(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint32_t color) {
  int16_t steep = abs(y1 - y0) > abs(x1 - x0);
  if (steep) {
    _swap_int16_t(x0, y0);
    _swap_int16_t(x1, y1);
  }
  if (x0 > x1) {
    _swap_int16_t(x0, x1);
    _swap_int16_t(y0, y1);
  }

  int16_t dx, dy;
  dx = x1 - x0;
  dy = abs(y1 - y0);

  int16_t err = dx / 2;
  int16_t ystep;

  if (y0 < y1) {
    ystep = 1;
  } else {
    ystep = -1;
  }

  for (; x0<=x1; x0++) {
    if (steep) {
      GFX_drawPixel_A(y0, x0, color);
    } else {
      GFX_drawPixel_A(x0, y0, color);
    }
    err -= dy;
    if (err < 0) {
      y0 += ystep;
      err += dx;
    }
  }
}

void GFX_fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color) {
  for (int16_t i=x; i<x+w; i++) {
    GFX_drawLine(i, y, i, y+h-1, color);
  }
}

void GFX_fillRect_A(int16_t x, int16_t y, int16_t w, int16_t h, uint32_t color) {
  for (int16_t i=x; i<x+w; i++) {
    GFX_drawLine_A(i, y, i, y+h-1, color);
  }
}

void GFX_fillScreen(uint16_t color) {
  GFX_fillRect(0, 0, OLED_WIDTH, OLED_HEIGHT, color);
}

void GFX_init(char * displayPointer) {
  memset(displayBuffer, 0, sizeof(displayBuffer[0][0]) * OLED_WIDTH * OLED_HEIGHT);
  #if defined SCREEN_SSD1331
    mainDisplay = (SSD1331_handle_t*) displayPointer;
    SSD1331_begin(mainDisplay);
  #endif
}
