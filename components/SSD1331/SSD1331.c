#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "SSD1331.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"

void SSD1331_Rotate(SSD1331_handle_t *handle, int16_t *x, int16_t *y) {
  switch(handle->rotation) {
    case 1:
      *x = handle->width -*x - 1;
      *y = handle->height -*y - 1;
      break;
    default:
      break;
  }
}

void SSD1331_writeCommand(SSD1331_handle_t *handle, uint8_t c) {
  esp_err_t ret;
  gpio_set_level(handle->PIN_DS, 0);
  spi_transaction_t tr = {
    .flags = 0,
    .length = 8,
    .tx_buffer = &c,
  };
  ret = spi_device_transmit(*handle->spi, &tr);
  assert(ret == ESP_OK);
}

void SSD1331_writeData(SSD1331_handle_t *handle, uint8_t c) {
  esp_err_t ret;
  gpio_set_level(handle->PIN_DS, 1);
  spi_transaction_t tr = {
    .flags = 0,
    .length = 8,
    .tx_buffer = &c,
  };
  ret = spi_device_transmit(*handle->spi, &tr);
  assert(ret == ESP_OK);
}

void SSD1331_goTo(SSD1331_handle_t *handle, int16_t x, int16_t y) {
  if ((x >= handle->width) || (y >= handle->height) || (x < 0) || (y < 0)) return;
  SSD1331_Rotate(handle, &x, &y);
  SSD1331_writeCommand(handle, SSD1331_CMD_SETCOLUMN);
  SSD1331_writeCommand(handle, x);
  SSD1331_writeCommand(handle, handle->width - 1);
  SSD1331_writeCommand(handle, SSD1331_CMD_SETROW);
  SSD1331_writeCommand(handle, y);
  SSD1331_writeCommand(handle, handle->height - 1);
}

void SSD1331_clearWindowUnrotated(SSD1331_handle_t *handle, int16_t x0, int16_t y0, int16_t x1, int16_t y1) {
  SSD1331_writeCommand(handle, SSD1331_CMD_CLEARWINDOW);
  SSD1331_writeCommand(handle, x0);
  SSD1331_writeCommand(handle, y0);
  SSD1331_writeCommand(handle, x1);
  SSD1331_writeCommand(handle, y1);
  vTaskDelay(SSD1331_DELAYS_HWFILL/portTICK_PERIOD_MS);
}

void SSD1331_clearWindow(SSD1331_handle_t *handle, int16_t x0, int16_t y0, int16_t x1, int16_t y1) {
  SSD1331_Rotate(handle, &x0, &y0);
  SSD1331_Rotate(handle, &x1, &y1);
  SSD1331_clearWindowUnrotated(handle, x0, y0, x1, y1);
}

void SSD1331_clearScreen(SSD1331_handle_t *handle) {
  SSD1331_clearWindowUnrotated(handle, 0, 0, handle->width - 1, handle->height - 1);
  //SSD1331_writeCommand(handle, SSD1331_CMD_DISPLAYALLOFF);
}

void SSD1331_drawLine(SSD1331_handle_t *handle, int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color) {
  if((y0 >= handle->height) && (y1 >= handle->height))
    return;
  if((x0 >= handle->width) && (x1 >= handle->width))
    return;
  if (x0 >= handle->width)
    x0 = handle->width - 1;
  if (y0 >= handle->height)
    y0 = handle->height - 1;
  if (x1 >= handle->width)
    x1 = handle->width - 1;
  if (y1 >= handle->height)
    y1 = handle->height - 1;
  SSD1331_Rotate(handle, &x0, &y0);
  SSD1331_Rotate(handle, &x1, &y1);
  SSD1331_writeCommand(handle, SSD1331_CMD_DRAWLINE);  	// 0xAE
  SSD1331_writeCommand(handle, x0);
  SSD1331_writeCommand(handle, y0);
  SSD1331_writeCommand(handle, x1);
  SSD1331_writeCommand(handle, y1);
  vTaskDelay(SSD1331_DELAYS_HWLINE/portTICK_PERIOD_MS);
  SSD1331_writeCommand(handle, (uint8_t)((color >> 11) << 1));
  SSD1331_writeCommand(handle, (uint8_t)((color >> 5) & 0x3F));
  SSD1331_writeCommand(handle, (uint8_t)((color << 1) & 0x3F));
  vTaskDelay(SSD1331_DELAYS_HWLINE/portTICK_PERIOD_MS);
}

void SSD1331_pushColor(SSD1331_handle_t *handle, uint16_t color) {
  SSD1331_writeData(handle, (uint8_t)((color >> 11) << 1));
  SSD1331_writeData(handle, (uint8_t)((color >> 5) & 0x3F));
  SSD1331_writeData(handle, (uint8_t)((color << 1) & 0x3F));
  //SSD1331_writeData(handle, color >> 8);
  //SSD1331_writeData(handle, color);
}

void SSD1331_drawPixel(SSD1331_handle_t *handle, int16_t x, int16_t y, uint16_t color) {
  if ((x >= handle->width) || (y >= handle->height) || (x < 0) || (y < 0)) return; //fix for pushing too much data
  //SSD1331_drawLine(handle, x, y, x, y, color);
  SSD1331_goTo(handle, x, y);
  SSD1331_pushColor(handle, color);
}

void SSD1331_begin(SSD1331_handle_t *handle) {
  gpio_set_direction(handle->PIN_DS, GPIO_MODE_OUTPUT);
  gpio_set_direction(handle->PIN_RST, GPIO_MODE_OUTPUT);

  gpio_set_level(handle->PIN_RST, 1);
  vTaskDelay(1/portTICK_PERIOD_MS);
  gpio_set_level(handle->PIN_RST, 0);
  vTaskDelay(10/portTICK_PERIOD_MS);
  gpio_set_level(handle->PIN_RST, 1);

  //Init sequence
  SSD1331_writeCommand(handle, SSD1331_CMD_DISPLAYOFF);  	// 0xAE
  SSD1331_writeCommand(handle, SSD1331_CMD_SETREMAP); 	// 0xA0
  SSD1331_writeCommand(handle, 0xA0); // RGB Color
  SSD1331_writeCommand(handle, SSD1331_CMD_STARTLINE); 	// 0xA1
  SSD1331_writeCommand(handle, 0x0);
  SSD1331_writeCommand(handle, SSD1331_CMD_DISPLAYOFFSET); 	// 0xA2
  SSD1331_writeCommand(handle, 0x0);
  SSD1331_writeCommand(handle, SSD1331_CMD_NORMALDISPLAY);  	// 0xA4
  SSD1331_writeCommand(handle, SSD1331_CMD_SETMULTIPLEX); 	// 0xA8
  SSD1331_writeCommand(handle, 0x3F);  			// 0x3F 1/64 duty
  SSD1331_writeCommand(handle, SSD1331_CMD_SETMASTER);  	// 0xAD
  SSD1331_writeCommand(handle, 0x8E);
  SSD1331_writeCommand(handle, SSD1331_CMD_POWERMODE);  	// 0xB0
  SSD1331_writeCommand(handle, 0x0B);
  SSD1331_writeCommand(handle, SSD1331_CMD_PRECHARGE);  	// 0xB1
  SSD1331_writeCommand(handle, 0x31);
  SSD1331_writeCommand(handle, SSD1331_CMD_CLOCKDIV);  	// 0xB3
  SSD1331_writeCommand(handle, 0xD0);  // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)
  SSD1331_writeCommand(handle, SSD1331_CMD_PRECHARGEA);  	// 0x8A
  SSD1331_writeCommand(handle, 0x64);
  SSD1331_writeCommand(handle, SSD1331_CMD_PRECHARGEB);  	// 0x8B
  SSD1331_writeCommand(handle, 0x78);
  SSD1331_writeCommand(handle, SSD1331_CMD_PRECHARGEC);  	// 0x8C
  SSD1331_writeCommand(handle, 0x64);
  SSD1331_writeCommand(handle, SSD1331_CMD_PRECHARGELEVEL);  	// 0xBB
  SSD1331_writeCommand(handle, 0x3A);
  SSD1331_writeCommand(handle, SSD1331_CMD_VCOMH);  		// 0xBE
  SSD1331_writeCommand(handle, 0x3E);
  SSD1331_writeCommand(handle, SSD1331_CMD_MASTERCURRENT);  	// 0x87
  SSD1331_writeCommand(handle, 0x06);
  SSD1331_writeCommand(handle, SSD1331_CMD_CONTRASTA);  	// 0x81
  SSD1331_writeCommand(handle, 0x91);
  SSD1331_writeCommand(handle, SSD1331_CMD_CONTRASTB);  	// 0x82
  SSD1331_writeCommand(handle, 0x50);
  SSD1331_writeCommand(handle, SSD1331_CMD_CONTRASTC);  	// 0x83
  SSD1331_writeCommand(handle, 0x7D);
  SSD1331_writeCommand(handle, SSD1331_CMD_DISPLAYON); //--turn on oled panel
}
